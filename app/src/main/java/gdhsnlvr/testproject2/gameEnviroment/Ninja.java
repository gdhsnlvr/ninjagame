package gdhsnlvr.testproject2.gameEnviroment;

import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;

/**
 * Created by gdhsnlvr on 19.09.16.
 */
public class Ninja extends Object {
    static public final int WIDTH = 32;
    static public final int HEIGHT = 32;
    protected float _lastJumpPosition;
    protected boolean _isOnJump;
    protected boolean _jumpBlocked;
    protected boolean _rotating;
    protected boolean _alive;

    public Ninja(PointF origin) {
        super(origin, new PointF(WIDTH, HEIGHT));
        _alive = true;
        _isOnJump = false;
        _jumpBlocked = false;
        _rotating = false;
    }

    public boolean isOnJump() {
        return _isOnJump;
    }

    public void setIsOnJump(boolean isOnJump) {
        _isOnJump = isOnJump;
        if (_isOnJump) {
            _lastJumpPosition = _origin.x;
        }
    }

    public boolean isJumpBlocked() {
        return _jumpBlocked;
    }

    public void setJumpBlocked(boolean jumpBlocked) {
        _jumpBlocked = jumpBlocked;
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        if (_origin.y < 0) {
            _origin.y = 0;
            _speed.y = 0;
            _isOnJump = false;
        }
    }

    public boolean isRotating() {
        return _rotating;
    }

    public void setRotating(boolean rotating) {
        _rotating = rotating;
    }

    public boolean isAlive() {
        return _alive;
    }

    public void setAlive(boolean alive) {
        _alive = alive;
    }

    public float getLastJumpPosition() {
        return _lastJumpPosition;
    }
}
