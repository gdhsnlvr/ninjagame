package gdhsnlvr.testproject2.gameEnviroment;

import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;

/**
 * Created by gdhsnlvr on 19.09.16.
 */
public class Barrier extends Object {
    static public final int WIDTH = 16;
    static public final int HEIGHT = 36;

    public Barrier(PointF origin, PointF size) {
        super(origin, size);
    }
}
