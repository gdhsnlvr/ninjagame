package gdhsnlvr.testproject2.gameEnviroment;

import android.graphics.Point;
import android.graphics.PointF;

import java.util.ArrayList;

import gdhsnlvr.testproject2.gameEngine.Scene;

/**
 * Created by gdhsnlvr on 22.09.16.
 */
public class BarrierGenerator {
    protected RunningTrack _runningTrack;
    protected Camera _camera;
    protected Scene _scene;
    protected static final float PREFERRED_SPICE_LENGTH = Ninja.WIDTH * 1.0f;
    protected static final float PREFERRED_ROTATION_LENGTH = Ninja.WIDTH * 20.0f;
    protected static final float SAFE_SPICE_LENGTH_BEFORE_ROTATION = Ninja.WIDTH * 4.0f;

    public BarrierGenerator(Camera camera, RunningTrack runningTrack, Scene scene) {
        _runningTrack = runningTrack;
        _camera = camera;
        _scene = scene;
    }

    public float generateRotationPoint() {
        final float nextRotation = _scene.getDivisionLength();
        return Math.max(nextRotation, 0) + (float) Math.random() * PREFERRED_ROTATION_LENGTH + PREFERRED_ROTATION_LENGTH;
    }

    public ArrayList<Barrier> generateNewBarriers() {
        ArrayList<Barrier> barriers = new ArrayList<>();
        Barrier farthestBarrier = _runningTrack.getFarthestBarrier();
        /*final float nextRotation = _runningTrack.getFarthestRotationPoint();
        if (farthestBarrier == null || farthestBarrier._origin.x < _runningTrack.getLengthPassed() + _camera.getRelativeWidth() + _camera.getRelativeHeight()) {
            float lastOffset = farthestBarrier == null ? _camera.getRelativeWidth() : farthestBarrier.getOrigin().x + farthestBarrier.getSize().x;
            float restLength = _runningTrack.getFarthestRotationPoint() - (_runningTrack.getLengthPassed() + _camera.getRelativeWidth() + _camera.getRelativeHeight());
            int countOfScreens = (int) Math.ceil(restLength / _camera.getRelativeWidth());
            int countOfBarriers = countOfScreens * 2 + (int) Math.round(Math.random() * countOfScreens);
            for (int i = 0; i < 10; i++) {
                float offset = generateOffset(lastOffset);
                Barrier barrier = new Barrier(new PointF(offset, 0), new PointF(Barrier.WIDTH, Barrier.HEIGHT));

                //if (isOnSafeZone(barrier, nextRotation) && barrier.getOrigin().x < nextRotation) {
                    barriers.add(barrier);
                    lastOffset = offset;
                //}
            }
        }*/
        return barriers;
    }

    protected float generateOffset(float lastOffset) {
        return lastOffset + (float) Math.random() * PREFERRED_SPICE_LENGTH * 1.5f + PREFERRED_SPICE_LENGTH;
    }

    protected boolean intersects(PointF segment, PointF otherSegment) {
        float leftCoordinate = Math.max(segment.x, otherSegment.x);
        float rightCoordinate = Math.min(segment.y, otherSegment.y);
        return rightCoordinate > leftCoordinate;
    }

    protected boolean isOnSafeZone(Barrier barrier, float nextRotation) {
        return !intersects(new PointF(barrier.getOrigin().x, barrier.getOrigin().x + barrier.getSize().x),
                new PointF(nextRotation - SAFE_SPICE_LENGTH_BEFORE_ROTATION, nextRotation + SAFE_SPICE_LENGTH_BEFORE_ROTATION));
    }
}
