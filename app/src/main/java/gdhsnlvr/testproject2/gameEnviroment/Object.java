package gdhsnlvr.testproject2.gameEnviroment;

import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;

/**
 * Created by gdhsnlvr on 21.09.16.
 */
public class Object {
    protected PointF _origin;
    protected PointF _size;
    protected PointF _speed;

    Object(PointF origin, PointF size) {
        _origin = origin;
        _size = size;
        _speed = new PointF(0, 0);
    }

    public void update(float deltaTime) {
        translate(_speed.x * deltaTime, _speed.y * deltaTime);
    }

    public void translate(float dx, float dy) {
        _origin.x += dx;
        _origin.y += dy;
    }

    public void setPosition(float x, float y) {
        _origin = new PointF(x, y);
    }

    public void setSize(float width, float height) {
        _size = new PointF(width, height);
    }

    public void setSpeed(float vx, float vy) {
        _speed = new PointF(vx, vy);
    }

    public PointF getSize() {
        return _size;
    }

    public PointF getOrigin() {
        return _origin;
    }

    public PointF getSpeed() {
        return _speed;
    }

    public RectF getBoundary() {
        return new RectF(_origin.x, _origin.y + _size.y, _origin.x + _size.x, _origin.y);
    }

    public boolean collide(Object object) {
        return getBoundary().intersect(object.getBoundary());
    }
}
