package gdhsnlvr.testproject2.gameEnviroment;

import java.util.ArrayList;

/**
 * Created by gdhsnlvr on 19.09.16.
 */
public class RunningTrack {
    private ArrayList<Barrier> _barriers;
    private ArrayList<Float> _rotationPoints;

    public RunningTrack() {
        _barriers = new ArrayList<>();
        _rotationPoints = new ArrayList<>();
    }

    public ArrayList<Barrier> getBarriers() {
        return _barriers;
    }

    public Barrier getFarthestBarrier() {
        if (_barriers.isEmpty()) {
            return null;
        }
        return _barriers.get(_barriers.size() - 1);
    }

    public ArrayList<Float> getRotationPoints() {
        return _rotationPoints;
    }
}