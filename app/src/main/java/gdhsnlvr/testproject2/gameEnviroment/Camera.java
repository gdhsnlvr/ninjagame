package gdhsnlvr.testproject2.gameEnviroment;

import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;

/**
 * Created by gdhsnlvr on 21.09.16.
 */
public class Camera extends Object {
    private int _rotation;
    private float _distanceTraveled;

    public Camera(PointF origin, PointF size) {
        super(origin, size);
        _rotation = 0;
        _distanceTraveled = 0;
    }

    public int getRotation() {
        return _rotation;
    }

    public void setRotation(int rotation) {
        _rotation = rotation;
    }

    public float getRelativeWidth() {
        if (_rotation == 1 || _rotation == 3)
            return _size.y;
        return _size.x;
    }

    public float getRelativeHeight() {
        if (_rotation == 1 || _rotation == 3)
            return _size.x;
        return _size.y;
    }

    public float getDistanceTraveled() {
        return _distanceTraveled;
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        _distanceTraveled += _speed.x * deltaTime + _speed.y * deltaTime;
    }
}
