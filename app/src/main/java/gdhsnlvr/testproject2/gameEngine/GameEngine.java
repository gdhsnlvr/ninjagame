package gdhsnlvr.testproject2.gameEngine;

import android.graphics.PointF;
import android.util.Log;

import java.util.ArrayList;

import gdhsnlvr.testproject2.gameEnviroment.Barrier;
import gdhsnlvr.testproject2.gameEnviroment.BarrierGenerator;
import gdhsnlvr.testproject2.gameEnviroment.Camera;
import gdhsnlvr.testproject2.gameEnviroment.Ninja;
import gdhsnlvr.testproject2.gameEnviroment.RunningTrack;

/**
 * Created by gdhsnlvr on 20.09.16.
 */
public class GameEngine {
    private static final float NINJA_SPEED = Ninja.HEIGHT * 4.f / 1000.f;
    private static final float JUMP_HEIGHT = 2.4f * Ninja.HEIGHT;
    private static final float JUMP_WIDTH = 2.0f * Ninja.WIDTH;
    private static final float JUMP_TIME = JUMP_WIDTH / NINJA_SPEED / 2.0f;
    private static final float JUMP_SPEED = 2.0f * JUMP_HEIGHT / JUMP_TIME;
    private static final float GRAVITY = 2.0f * JUMP_HEIGHT / (JUMP_TIME * JUMP_TIME);
    private static final float ROTATION_ZONE_LENGTH = 2.0f * Ninja.WIDTH;

    private Scene _scene;
    private Camera _camera;
    private Ninja _ninja;
    private RunningTrack _runningTrack;
    private BarrierGenerator _barrierGenerator;
    private float _totalTime;
    private float _totalLength;
    private boolean _rotatingReached;
    private boolean _ninjaRotating;
    private float _currentRotationPoint;

    private boolean _cameraStoped;

    public GameEngine() {
        _ninja = new Ninja(new PointF(Ninja.WIDTH / 2.0f, 0));
        _runningTrack = new RunningTrack();
        _rotatingReached = false;
        _ninjaRotating = false;
        _cameraStoped = false;
        _currentRotationPoint = -1;

        _scene = new Scene();
        _camera = new Camera(new PointF(0, 0), new PointF(Ninja.WIDTH * 8, Ninja.HEIGHT * 6));
        _barrierGenerator = new BarrierGenerator(_camera, _runningTrack, _scene);

        _scene.setNinja(_ninja);
        _scene.setRunningTrack(_runningTrack);
        _scene.setDivisionLength(-1);

        float rotation = _barrierGenerator.generateRotationPoint();
        _scene.setDivisionLength(rotation);
        _scene.rotateOver(rotation);

        _totalTime = 0;
        _totalLength = 0;

        float last = _camera.getRelativeWidth();
        for (int i = 0; i < 1000; i++) {
            Barrier barrier = new Barrier(new PointF(last + (float) Math.min(Math.random() * 10.0 * Ninja.WIDTH, 5 * Ninja.WIDTH), 0), new PointF(Barrier.WIDTH, Barrier.HEIGHT));
            _runningTrack.getBarriers().add(barrier);
            last = barrier.getOrigin().x;
        }
    }

    public void update(float deltaTime) {
        if (!_ninja.isAlive()) {
            return;
        }

        generateNewEnviroment();
        updateScene(deltaTime);
        checkForRotation();
    }

    protected void checkForRotation() {
        boolean needStopCamera = false;
        PointF rotationPoint = _scene.previousToWorld(_scene.getDivisionLength());
        if (_camera.getRotation() == 0) {
            needStopCamera = _camera.getOrigin().x + _camera.getSize().x > rotationPoint.x;
        } else if (_camera.getRotation() == 1) {
            needStopCamera = _camera.getOrigin().y + _camera.getSize().y > rotationPoint.y;
        } else if (_camera.getRotation() == 2) {
            needStopCamera = _camera.getOrigin().x < rotationPoint.x;
        } else {
            needStopCamera = _camera.getOrigin().y < rotationPoint.y;
        }

        if (!_rotatingReached && needStopCamera) {
            _rotatingReached = true;

            if (_camera.getRotation() == 0) {
                PointF position = _scene.previousToWorld(_scene.getDivisionLength() - _camera.getSize().x);
                _camera.setPosition(position.x, position.y);
            } else if (_camera.getRotation() == 1) {
                PointF position = _scene.previousToWorld(_scene.getDivisionLength() - _camera.getSize().y);
                _camera.setPosition(position.x - _camera.getSize().x, position.y);
            } else if (_camera.getRotation() == 2) {
                PointF position = _scene.previousToWorld(_scene.getDivisionLength() - _camera.getSize().x);
                _camera.setPosition(position.x - _camera.getSize().x, position.y - _camera.getSize().y);
            } else {
                PointF position = _scene.previousToWorld(_scene.getDivisionLength() - _camera.getSize().x);
                _camera.setPosition(position.x, position.y - _camera.getRelativeHeight());
            }
            _camera.setRotation((_camera.getRotation() + 1) % 4);
            _cameraStoped = true;
        }

        if (_cameraStoped && _ninja.getOrigin().x > _scene.getDivisionLength()) {
            _cameraStoped = false;
        }

        if (_rotatingReached && _ninja.getOrigin().x > _scene.getDivisionLength() + _camera.getRelativeWidth()) {
            _rotatingReached = false;
            float rotation = _barrierGenerator.generateRotationPoint();
            _scene.setDivisionLength(rotation);
            _scene.rotateOver(rotation);
        }
    }

    protected void generateNewEnviroment() {

    }

    protected void updateScene(float deltaTime) {
        float speed = Ninja.HEIGHT * 4.f / 1000.f + speedByTime(_totalTime);
        PointF speedVector = speedByRotation(_camera.getRotation());

        if (!_cameraStoped) {
            _camera.setSpeed(speed * speedVector.x, speed * speedVector.y);
        } else {
            _camera.setSpeed(0, 0);
        }

        float ninjaSpeedY = _ninja.getSpeed().y;
        _ninja.setSpeed(speed, ninjaSpeedY - GRAVITY * deltaTime);
        _ninja.update(deltaTime);
        _camera.update(deltaTime);

        _totalTime += deltaTime;
        _totalLength += speed * deltaTime;
    }

    public void jumpRequest() {
        if (_ninjaRotating || _ninja.isOnJump() || _ninja.isJumpBlocked()) {
            return;
        }

        PointF speed = _ninja.getSpeed();
        _ninja.setSpeed(speed.x, JUMP_SPEED);
        _ninja.setIsOnJump(true);
    }

    public Scene getScene() {
        return _scene;
    }

    public Camera getCamera() {
        return _camera;
    }

    protected float speedByTime(float time) {
        return (float) Math.sqrt(time / 1000.f / 60.0f) * Ninja.HEIGHT * 4.f / 1000.f;
    }

    protected PointF speedByRotation(int rotation)
    {
        if (rotation == 0) {
            return new PointF(1, 0);
        } else if (rotation == 1) {
            return new PointF(0, 1);
        } else if (rotation == 2) {
            return new PointF(-1, 0);
        } else  {
            return new PointF(0, -1);
        }
    }
}
