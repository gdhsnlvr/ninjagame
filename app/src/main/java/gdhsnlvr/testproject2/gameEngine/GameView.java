package gdhsnlvr.testproject2.gameEngine;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

/**
 * Created by gdhsnlvr on 20.09.16.
 */
public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder _surfaceHolder;
    private SceneRenderer _sceneRenderer;
    private Context _context;
    private GameThread _gameThread;
    private GameEngine _gameEngine;

    public GameView(GameEngine gameEngine, Context context) {
        super(context);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setFocusable(true);

        _context = context;
        _gameEngine = gameEngine;
        _sceneRenderer = new SceneRenderer(_gameEngine.getCamera(), _gameEngine.getScene(), _context);
        _gameThread = new GameThread(holder, _context);
        _gameThread.setGameEngine(_gameEngine);
        _gameThread.setSceneRenderer(_sceneRenderer);

        updateScreenMetrics();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            _gameEngine.jumpRequest();
        }
        return super.onTouchEvent(event);
    }

    private void updateScreenMetrics() {
        WindowManager wm = (WindowManager) _context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        _sceneRenderer.setScreenWidth(width);
        _sceneRenderer.setScreenHeight(height);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if(!_gameThread.isRunning()) {
            _gameThread = new GameThread(holder, _context);
            _gameThread.setGameEngine(_gameEngine);
            _gameThread.setSceneRenderer(_sceneRenderer);
            _gameThread.start();
        } else {
            _gameThread.start();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        _sceneRenderer.setScreenWidth(width);
        _sceneRenderer.setScreenHeight(height);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        _gameThread.setIsRunning(false);
        boolean retry = true;
        while (retry)
        {
            try
            {
                _gameThread.join();
                retry = false;
            }
            catch (InterruptedException e) {}
        }
    }
}
