package gdhsnlvr.testproject2.gameEngine;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.SystemClock;
import android.view.SurfaceHolder;

/**
 * Created by gdhsnlvr on 20.09.16.
 */
public class GameThread extends Thread {
    private GameEngine _gameEngine;
    private SurfaceHolder _surfaceHolder;
    private SceneRenderer _sceneRenderer;
    private long _delayTime;
    private boolean  _isOnRun;

    private static final long FPS = 100;

    public GameThread(SurfaceHolder surfaceHolder, Context context) {
        _surfaceHolder = surfaceHolder;
        _isOnRun = true;
        _delayTime = 1000 / FPS;
    }

    public void setGameEngine(GameEngine gameEngine) {
        _gameEngine = gameEngine;
    }

    public void setSceneRenderer(SceneRenderer sceneRenderer) {
        _sceneRenderer = sceneRenderer;
    }

    @Override
    public void run() {
        long lastTime = System.currentTimeMillis();
        while (_isOnRun) {
            long currentTime = System.currentTimeMillis();
            long deltaTime = currentTime - lastTime;
            lastTime = currentTime;
            _gameEngine.update(deltaTime);

            Canvas canvas = _surfaceHolder.lockCanvas(null);
            if (canvas != null) {
                synchronized (_surfaceHolder) {
                    _sceneRenderer.render(canvas);
                }
                _surfaceHolder.unlockCanvasAndPost(canvas);
            }

            try {
                Thread.sleep(_delayTime);
            }
            catch (InterruptedException ex) {

            }
        }
    }

    public boolean isRunning() {
        return _isOnRun;
    }

    public void setIsRunning(boolean state) {
        _isOnRun = state;
    }
}
