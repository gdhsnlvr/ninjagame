package gdhsnlvr.testproject2.gameEngine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Shader;

import gdhsnlvr.testproject2.R;
import gdhsnlvr.testproject2.gameEnviroment.Barrier;
import gdhsnlvr.testproject2.gameEnviroment.Camera;
import gdhsnlvr.testproject2.gameEnviroment.Ninja;
import gdhsnlvr.testproject2.math.MathMatrix;

/**
 * Created by gdhsnlvr on 20.09.16.
 */
public class SceneRenderer {
    private Scene _scene;
    private Camera _camera;
    private int _screenWidth;
    private int _screenHeight;
    private Paint _paint;
    private Context _context;

    private int _frameCounter;
    private long _lastFpsTime;
    private double _fps;

    private Bitmap _foreground;
    private Bitmap _foregroundScaled;

    SceneRenderer(Camera camera, Scene scene, Context context) {
        _scene = scene;
        _camera = camera;
        _context = context;
        _paint = new Paint();
        _paint.setARGB(255, 255, 255, 255);

        _frameCounter = 0;
        _lastFpsTime = 0;
        _fps = 0;

        _foreground = BitmapFactory.decodeResource(_context.getResources(), R.drawable.city);
        _foregroundScaled = null;
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth)
    {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = (scaleWidth * height) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    protected RectF mapToScreen(RectF boundary) {
        float scaleY = _screenHeight / _camera.getSize().y;
        float scaleX = _screenWidth / _camera.getSize().x;
        RectF mapped = new RectF(boundary);
        mapped.left -= _camera.getOrigin().x;
        mapped.right -= _camera.getOrigin().x;
        mapped.top -= _camera.getOrigin().y;
        mapped.bottom -= _camera.getOrigin().y;
        mapped.left *= scaleX;
        mapped.right *= scaleX;
        mapped.top *= scaleY;
        mapped.bottom *= scaleY;
        float top = mapped.top;
        mapped.top = _screenHeight - mapped.bottom;
        mapped.bottom = _screenHeight - top;
        if (mapped.top > mapped.bottom) {
            float temp = mapped.top;
            mapped.top = mapped.bottom;
            mapped.bottom = temp;
        }
        if (mapped.left > mapped.right) {
            float temp = mapped.left;
            mapped.left = mapped.right;
            mapped.right = temp;
        }
        return mapped;
    }

    void render(Canvas canvas) {
        canvas.drawColor(Color.argb(255, 240, 248, 255));

        if (_foregroundScaled == null) {
            _foregroundScaled = getResizedBitmap(_foreground, _screenWidth);
        }

        Paint p = new Paint();
        p.setShader(new LinearGradient(0, 0, 0, _screenHeight, Color.argb(255, 240, 248, 255),
                Color.argb(255, 255, 243, 227), Shader.TileMode.MIRROR));
        canvas.drawPaint(p);
        canvas.drawBitmap(_foregroundScaled, 0, 0, _paint);

        renderHUD(canvas);
        renderScene(canvas);
    }

    protected void renderScene(Canvas canvas) {
        _paint.setARGB(255, 255, 255, 255);
        renderNinja(canvas, _scene.getNinja());

        _paint.setARGB(255, 255, 0, 0);

        for (Barrier barrier : _scene.getRunningTrack().getBarriers()) {
            renderBarrier(canvas, barrier);
        }
        for (float rotationPoint : _scene.getRunningTrack().getRotationPoints()) {
            renderRotationPoint(canvas, rotationPoint);
        }
    }

    protected void renderNinja(Canvas canvas, Ninja ninja) {
        RectF worldBoundary = new RectF(ninja.getBoundary());
        MathMatrix topLeftCorner = MathMatrix.createVector(new PointF(worldBoundary.left, worldBoundary.top));
        MathMatrix bottomRightCorner = MathMatrix.createVector(new PointF(worldBoundary.right, worldBoundary.bottom));
        if (ninja.getOrigin().x > _scene.getDivisionLength()) {
            topLeftCorner = topLeftCorner.multiply(_scene.getRotationMatrix());
            bottomRightCorner = bottomRightCorner.multiply(_scene.getRotationMatrix());
        } else {
            topLeftCorner = topLeftCorner.multiply(_scene.getPreviousRotationMatrix());
            bottomRightCorner = bottomRightCorner.multiply(_scene.getPreviousRotationMatrix());
        }
        worldBoundary.left = (float) topLeftCorner.get(0, 0);
        worldBoundary.top = (float) topLeftCorner.get(0, 1);
        worldBoundary.right = (float) bottomRightCorner.get(0, 0);
        worldBoundary.bottom = (float) bottomRightCorner.get(0, 1);
        RectF boundary = mapToScreen(worldBoundary);
        _paint.setARGB(255, 0, 255, 0);
        canvas.drawRect(boundary, _paint);
    }

    protected void renderBarrier(Canvas canvas, Barrier barrier) {
        RectF worldBoundary = new RectF(barrier.getBoundary());
        MathMatrix topLeftCorner = MathMatrix.createVector(new PointF(worldBoundary.left, worldBoundary.top));
        MathMatrix bottomRightCorner = MathMatrix.createVector(new PointF(worldBoundary.right, worldBoundary.bottom));
        //Log.i("dd", barrier.getOrigin().x + " " + _scene.getDivisionLength());
        if (barrier.getOrigin().x > _scene.getDivisionLength()) {
            topLeftCorner = topLeftCorner.multiply(_scene.getRotationMatrix());
            bottomRightCorner = bottomRightCorner.multiply(_scene.getRotationMatrix());
        } else {
            topLeftCorner = topLeftCorner.multiply(_scene.getPreviousRotationMatrix());
            bottomRightCorner = bottomRightCorner.multiply(_scene.getPreviousRotationMatrix());
        }
        worldBoundary.left = (float) topLeftCorner.get(0, 0);
        worldBoundary.top = (float) topLeftCorner.get(0, 1);
        worldBoundary.right = (float) bottomRightCorner.get(0, 0);
        worldBoundary.bottom = (float) bottomRightCorner.get(0, 1);
        RectF boundary = mapToScreen(worldBoundary);
        _paint.setARGB(255, 255, 0, 0);
        canvas.drawRect(boundary, _paint);
    }

    protected void renderRotationPoint(Canvas canvas, float point) {
        RectF boundary = new RectF(point - 2, _camera.getRelativeHeight(), point, 0);
        RectF mapped = mapToScreen(boundary);
        _paint.setARGB(255, 255, 255, 255);
        canvas.drawRect(mapped, _paint);
    }

    protected void renderHUD(Canvas canvas) {
        _paint.setARGB(255, 0, 0, 0);
        _paint.setTextSize(30);
        canvas.drawText(String.format("%.2f", _fps), 10, 30, _paint);

        _frameCounter++;
        long delay = (int)(System.currentTimeMillis() - _lastFpsTime);
        if (delay > 1000) {
            _fps = (double) _frameCounter / delay * 1000.0;
            _frameCounter = 0;
            _lastFpsTime = System.currentTimeMillis();
        }

        _paint.setARGB(255, 0, 0, 0);

        if (!_scene.getNinja().isAlive()) {
            _paint.setTextSize(100);
            canvas.drawText("You are lost!", _screenWidth / 2.0f, _screenHeight / 2.0f, _paint);
        }
    }

    public int getScreenHeight() {
        return _screenHeight;
    }

    public void setScreenHeight(int screenHeight) {
        _screenHeight = screenHeight;
    }

    public int getScreenWidth() {
        return _screenWidth;
    }

    public void setScreenWidth(int screenWidth) {
        _screenWidth = screenWidth;
    }
}
