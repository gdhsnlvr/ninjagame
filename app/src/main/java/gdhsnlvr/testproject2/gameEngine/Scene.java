package gdhsnlvr.testproject2.gameEngine;

import android.graphics.PointF;

import gdhsnlvr.testproject2.gameEnviroment.Ninja;
import gdhsnlvr.testproject2.gameEnviroment.RunningTrack;
import gdhsnlvr.testproject2.math.MathMatrix;

/**
 * Created by gdhsnlvr on 20.09.16.
 */
public class Scene {
    private Ninja _ninja;
    private RunningTrack _runningTrack;
    private MathMatrix _rotationMathMatrix;
    private MathMatrix _previousRotationMathMatrix;
    private float _divisionLength;

    Scene() {
        _rotationMathMatrix = MathMatrix.createIdentity(3, 3);
        _previousRotationMathMatrix = MathMatrix.createIdentity(3, 3);
    }

    public Ninja getNinja() {
        return _ninja;
    }

    public void setNinja(Ninja ninja) {
        _ninja = ninja;
    }

    public RunningTrack getRunningTrack() {
        return _runningTrack;
    }

    public void setRunningTrack(RunningTrack runningTrack) {
        _runningTrack = runningTrack;
    }

    public MathMatrix getPreviousRotationMatrix() {
        return _previousRotationMathMatrix;
    }

    public MathMatrix getRotationMatrix() {
        return _rotationMathMatrix;
    }

    public float getDivisionLength() {
        return _divisionLength;
    }

    public void setDivisionLength(float _divisionLength) {
        this._divisionLength = _divisionLength;
    }

    public void rotateOver(float length) {
        MathMatrix rotation = MathMatrix.CreateRotation(length, 0, (float) Math.PI / 2.0f);
        _previousRotationMathMatrix = _rotationMathMatrix.getClone();
        _rotationMathMatrix = rotation.multiply(_rotationMathMatrix);
    }

    PointF previousToWorld(float length) {
        MathMatrix mathMatrix = MathMatrix.createVector(new PointF(length, 0));
        MathMatrix result = mathMatrix.multiply(_previousRotationMathMatrix);
        return new PointF((float) result.get(0, 0), (float) result.get(0, 1));
    }

    PointF rotateToWorld(float length) {
        MathMatrix mathMatrix = MathMatrix.createVector(new PointF(length, 0));
        MathMatrix result = mathMatrix.multiply(_rotationMathMatrix);
        return new PointF((float) result.get(0, 0), (float) result.get(0, 1));
    }
}
