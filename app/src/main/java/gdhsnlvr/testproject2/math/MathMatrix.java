package gdhsnlvr.testproject2.math;

import android.graphics.PointF;

/**
 * Created by gdhsnlvr on 25.09.16.
 */
public class MathMatrix {
    protected double _data[][];
    protected int _n, _m;

    public MathMatrix(int n, int m)
    {
        _data = new double[n][m];
        _n = n; _m = m;

        for (int i = 0; i < _n; i++) {
            for (int j = 0; j < _m; j++) {
                _data[i][j] = 0;
            }
        }
    }

    public static MathMatrix createIdentity(int n, int m)
    {
        MathMatrix mathMatrix = new MathMatrix(n, m);
        for (int i = 0; i < Math.min(n, m); i++) {
            mathMatrix._data[i][i] = 1;
        }
        return mathMatrix;
    }

    public static MathMatrix CreateRotation(float x, float y, float phi)
    {
        MathMatrix mathMatrix = new MathMatrix(3, 3);

        mathMatrix._data[0][0] = Math.cos(phi);
        mathMatrix._data[0][1] = Math.sin(phi);
        mathMatrix._data[0][2] = 0;

        mathMatrix._data[1][0] = - Math.sin(phi);
        mathMatrix._data[1][1] = Math.cos(phi);
        mathMatrix._data[1][2] = 0;

        mathMatrix._data[2][0] = - x * (Math.cos(phi) - 1.0f) + y * Math.sin(phi);
        mathMatrix._data[2][1] = - y  * (Math.cos(phi) - 1.0f) - x * Math.sin(phi);
        mathMatrix._data[2][2] = 1;

        return mathMatrix;
    }

    public static MathMatrix createVector(PointF point)
    {
        MathMatrix mathMatrix = new MathMatrix(1, 3);
        mathMatrix._data[0][0] = point.x;
        mathMatrix._data[0][1] = point.y;
        mathMatrix._data[0][2] = 1;
        return mathMatrix;
    }

    public MathMatrix multiply(MathMatrix mathMatrix)
    {
        MathMatrix result = new MathMatrix(_n, mathMatrix._m);

        for (int i = 0; i < _n; i++) {
            for (int j = 0; j < mathMatrix._m; j++) {
                for (int k = 0; k < _m; k++) {
                    result._data[i][j] += _data[i][k] * mathMatrix._data[k][j];
                }
            }
        }

        return result;
    }

    public MathMatrix getClone() {
        MathMatrix mathMatrix = new MathMatrix(_n, _m);
        for (int i = 0; i < _n; i++) {
            for (int j = 0; j < _m; j++) {
                mathMatrix._data[i][j] = _data[i][j];
            }
        }
        return mathMatrix;
    }

    public double get(int i, int j) {
        return _data[i][j];
    }
}
